package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class RectanguloActividad extends AppCompatActivity {
    private Rectangulo rectangulo;

    private EditText txtBase;
    private TextView lblBase;

    private TextView lblNombre;
    private TextView lblAltura;
    private EditText txtAltura;


    private TextView lblCalArea;

    private TextView lblCalPermitro;

    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo_actividad);

        lblNombre = (TextView) findViewById(R.id.lblNombre);
        lblCalArea = (TextView) findViewById(R.id.lblArea);


        txtBase = (EditText) findViewById(R.id.txtBase);
        txtAltura = (EditText) findViewById(R.id.txtAltura);


        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);



        Bundle datos = getIntent().getExtras();
        lblNombre.setText("Mi nombre es: " + (datos.getString("Nombre")));

        rectangulo = (Rectangulo) datos.getSerializable("rectangulo");



        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtBase.getText().toString().matches("")
                        || txtAltura.getText().toString().matches("")){
                    Toast.makeText(RectanguloActividad.this, "Falto Capturar", Toast.LENGTH_SHORT).show();
                }else{
                    rectangulo.setAltura(Integer.valueOf(txtAltura.getText().toString()));
                    rectangulo.setBase(Integer.valueOf(txtBase.getText().toString()));


                    lblCalArea.setText("Area: " +(String.valueOf(rectangulo.calcularArea())));
                    lblCalPermitro.setText("Perimetro: " + (String.valueOf(rectangulo.calcularPerimetro())));



                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtBase.setText("");
                txtAltura.setText("");
                lblNombre.setText("");
                lblBase.setText("");
                lblAltura.setText("");



            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(RectanguloActividad.this, MainActivity.class);
                //Enviar un dato String
                startActivity(intent);
            }
        });
    }

}
