package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Rectangulo rectangulo;
    private EditText txtNombre;
    private Button btnEntrar;
    private Button btnSalir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        btnEntrar = (Button) findViewById (R.id.btnEntrar);
        btnSalir = (Button) findViewById(R.id.btnSalir);

        rectangulo = new Rectangulo();


        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String cliente = txtNombre.getText().toString();

                if(cliente.matches("")){
                    Toast.makeText(MainActivity.this, "Falto Capturar Nombre", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(MainActivity.this, RectanguloActividad.class);
                    //Enviar un dato String
                    intent.putExtra("Nombre", cliente);

                    //Enviar un objeto

                    Bundle objeto = new Bundle();
                    objeto.putSerializable("rectangulo",rectangulo);

                    intent.putExtras(objeto);

                    startActivity(intent);

                }
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
