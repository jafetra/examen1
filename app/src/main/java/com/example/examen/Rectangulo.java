package com.example.examen;

import java.io.Serializable;
import java.util.Random;

public class Rectangulo implements Serializable {


    private int Base;
    private int Altura;
    private float calcularCalcularArea;
    private float calcularPagPerimetro;
    private int rectangulo;

    //Constructores

    public Rectangulo(int base, int altura, int rectangulo) {


        this.Base = base;
        this.Altura = altura;
        this.rectangulo = rectangulo;
    }

    public Rectangulo() {
        ;
    }

    //SET Y GET

    public int getAltura() {
        return Altura;
    }

    public int getBase() {
        return Base;
    }

    public float getCalcularPagPerimetro() {
        return calcularPagPerimetro;
    }

    public int getRectangulo() {
        return rectangulo;
    }


    public void setBase(int base) {
        this.Base = base;
    }

    public void setAltura(int altura) {
        this.Altura = altura;
    }

    public void setCalcularArea(float calcularArea) {
        this.calcularCalcularArea = calcularCalcularArea;
    }

    public void setCalcularPagPerimetro(float calcularPagPerimetro) {
        this.calcularPagPerimetro = calcularPagPerimetro;
    }

    public void setRectangulo(int rectangulo) {
        this.rectangulo = rectangulo;
    }


    // comportamiento



    public float calcularArea(){
        return this.Base*this.Altura;
    }

    public float calcularPerimetro (){
        float lado1 = this.Altura * Altura;
        float lado2 = this.Base * Base;

        return lado1 + lado2;
    }

}
